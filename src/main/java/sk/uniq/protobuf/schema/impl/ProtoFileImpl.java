/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import sk.uniq.protobuf.schema.ProtoSyntaxElement;
import sk.uniq.protobuf.schema.ProtoVersion;
import sk.uniq.protobuf.schema.ProtoFile;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import sk.uniq.protobuf.schema.ProtoComments;
import sk.uniq.protobuf.schema.ProtoEnum;
import sk.uniq.protobuf.schema.ProtoImport;
import sk.uniq.protobuf.schema.ProtoOption;
import sk.uniq.protobuf.schema.ProtoPosition;
import sk.uniq.protobuf.schema.ProtoService;
import sk.uniq.protobuf.schema.ProtoVersion.Version;

/**
 *
 * @author jherkel
 */
public final class ProtoFileImpl implements ProtoFile {

    private final String filename;
    private final List<ProtoSyntaxElement> elements;

    /**
     *
     * @param builder
     */
    public ProtoFileImpl(Builder builder) {

        List<ProtoVersion> tmpVersion = builder.elements.stream().filter(line -> line.getType() == ProtoSyntaxElement.ElementType.VERSION)
                .map(c -> (ProtoVersion) c).collect(Collectors.toList());
        if (tmpVersion.isEmpty() == true) {
            throw new IllegalArgumentException("Missing proto version");
        }
        if (tmpVersion.size() > 1) {
            throw new IllegalArgumentException("Only one version element is allowed");
        }
        this.filename = builder.filename;
        for (ProtoSyntaxElement element : builder.elements) {
            switch (element.getType()) {
                case ENUM:
                case MESSAGE:
                case OPTION:
                case IMPORT:
                case SERVICE:
                case VERSION:
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Nested element %s is not allowed here", element.getType()));

            }
        }
        this.elements = Collections.unmodifiableList(new ArrayList(builder.elements));
    }

    /**
     *
     * @return version of proto file
     */
    @Override
    public Version getVersion() {
        return this.<ProtoVersion>getFilteredNestedElements(ProtoSyntaxElement.ElementType.VERSION).get(0).getVersion();
    }

    /**
     *
     * @return
     */
    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public List<ProtoSyntaxElement> getNestedElements() {
        return elements;
    }

    @Override
    public <T extends ProtoSyntaxElement> List<T> getFilteredNestedElements(ProtoSyntaxElement.ElementType type) {
        return elements.stream().filter(line -> line.getType() == type)
                .map(c -> (T) c).collect(Collectors.toList());
    }

    @Override
    public List<ProtoSyntaxElement> getFilteredNestedElements(Set<ProtoSyntaxElement.ElementType> types) {
        return elements.stream().filter(line -> types.contains(line.getType()))
                .collect(Collectors.toList());
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public ProtoPosition getPosition() {
        return ProtoPosition.EMPTY_POSITION;
    }

    @Override
    public ProtoComments getComments() {
        return ProtoComments.EMPTY;
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.unmodifiableSet(EnumSet.of(ElementType.VERSION, ElementType.IMPORT,
                ElementType.MESSAGE, ElementType.ENUM, ElementType.SERVICE,ElementType.OPTION,ElementType.PACKAGE));
    }

    @Override
    public ElementType getType() {
        return ElementType.PROTO;
    }

    /**
     *
     */
    public static final class Builder {

        private String filename;
        private final List<ProtoSyntaxElement> elements = new ArrayList<>();

        private Builder() {
        }

        /**
         *
         * @return
         */
        public ProtoFile build() {
            return new ProtoFileImpl(this);
        }

        /**
         *
         * @param version
         * @return
         */
        public Builder version(Version version) {
            for (Iterator<ProtoSyntaxElement> it = elements.iterator(); it.hasNext();) {
                ProtoSyntaxElement element = it.next();
                if (element instanceof ProtoVersion) {
                    it.remove();
                }
            }
            this.elements.add(ProtoVersionImpl.builder().version(version).build());
            return this;
        }

        /**
         *
         * @param filename
         * @return
         */
        public Builder filename(String filename) {
            this.filename = filename;
            return this;
        }

        /**
         *
         * @param element
         * @return
         */
        public Builder addNestedElement(ProtoSyntaxElement element) {
            elements.add(element);
            return this;
        }

        /**
         *
         * @param elements
         * @return
         */
        public Builder addAllNestedElements(Collection<ProtoSyntaxElement> elements) {
            this.elements.addAll(elements);
            return this;
        }

        /**
         *
         * @param importPB
         * @return
         */
        public Builder addImport(ProtoImport importPB) {
            this.elements.add(importPB);
            return this;
        }
        
        /**
         *
         * @param service
         * @return
         */
        public Builder addService(ProtoService service) {
            this.elements.add(service);
            return this;
        }        
        
        /**
         *
         * @param message
         * @return
         */
        public Builder addMessage(ProtoService message) {
            this.elements.add(message);
            return this;
        }      
        
        /**
         *
         * @param enumPB
         * @return
         */
        public Builder addEnum(ProtoEnum enumPB) {
            this.elements.add(enumPB);
            return this;
        }      
        
        /**
         *
         * @param option
         * @return
         */
        public Builder addOption(ProtoOption option) {
            this.elements.add(option);
            return this;
        }           
    }
}
