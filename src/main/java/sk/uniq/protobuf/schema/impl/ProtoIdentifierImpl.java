/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import java.util.Collections;
import java.util.Set;
import sk.uniq.protobuf.schema.ProtoIdentifier;
import static sk.uniq.protobuf.schema.ProtoIdentifier.isValidFullIdentifier;
import static sk.uniq.protobuf.schema.ProtoIdentifier.isValidIdentifier;

/**
 *
 * @author jherkel
 */
public final class ProtoIdentifierImpl extends ProtoSyntaxElementImpl implements ProtoIdentifier {

    private final String identifier;
    private final boolean fullIdentifierFlag;

    private ProtoIdentifierImpl(Builder builder) {
        super(builder);
        if (builder.name == null) {
            throw new IllegalArgumentException("Empty identifier");
        }
        fullIdentifierFlag = builder.name.contains(".");
        if (fullIdentifierFlag) {
            if (isValidFullIdentifier(builder.name) == false) {
                throw new IllegalArgumentException("Invalid full identifier name:" + builder.name);
            }
        } else {
            if (isValidIdentifier(builder.name) == false) {
                throw new IllegalArgumentException("Invalid identifier name:" + builder.name);
            }
        }
        this.identifier = builder.name;
    }

    /**
     *
     * @return identifier name
     */
    @Override
    public String getName() {
        return identifier;
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.EMPTY_SET;
    }

    @Override
    public ElementType getType() {
        return ElementType.IDENTIFIER;
    }

    @Override
    public String[] getIdentifiers() {
        return fullIdentifierFlag == false ? new String[] {identifier} : identifier.split(".");
    }

    @Override
    public boolean isFullIdentifier() {
        return fullIdentifierFlag;
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private String name;

        private Builder() {
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(String name) {
            this.name = name;
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public ProtoIdentifier build() {
            return new ProtoIdentifierImpl(this);
        }
    }

}
