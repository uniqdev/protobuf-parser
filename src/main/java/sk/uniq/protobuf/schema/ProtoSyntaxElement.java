/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

import java.util.List;
import java.util.Set;

/**
 *
 * @author jherkel
 */
public interface ProtoSyntaxElement {

    public enum ElementType {
        ENUM,
        ENUM_FIELD,
        IDENTIFIER,
        IMPORT,
        MAP,
        MESSAGE,
        MESSAGE_FIELD,
        ONEOF,
        ONEOF_FIELD,
        OPTION,
        OPTION_NAME,
        PACKAGE,
        PROTO,
        RESERVED,
        RPC,
        SERVICE,
        VERSION,
    }

    /**
     *
     * @return position for appropriate element
     */
    public ProtoPosition getPosition();

    /**
     *
     * @return comments for appropriate element
     */
    public ProtoComments getComments();

    public List<ProtoSyntaxElement> getNestedElements();

    public <T extends ProtoSyntaxElement> List<T> getFilteredNestedElements(ElementType type);

    public List<ProtoSyntaxElement> getFilteredNestedElements(Set<ElementType> types);

    public Set<ElementType> getAllowedElements();

    public ElementType getType();
    
}
