/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import java.util.Collections;
import java.util.Set;
import sk.uniq.protobuf.schema.ProtoVersion;

/**
 *
 * @author jherkel
 */
public class ProtoVersionImpl extends ProtoSyntaxElementImpl implements ProtoVersion {

    private final ProtoVersion.Version version;

    private ProtoVersionImpl(Builder builder) {
        super(builder);
        if (builder.version == null) {
            throw new IllegalArgumentException("Invalid version, version = null");
        }
        this.version = builder.version;
    }

    @Override
    public ProtoVersion.Version getVersion() {
        return version;
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.EMPTY_SET;
    }

    @Override
    public ElementType getType() {
        return ElementType.VERSION;
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private ProtoVersion.Version version;

        private Builder() {
        }

        /**
         *
         * @param version
         * @return
         */
        public Builder version(ProtoVersion.Version version) {
            this.version = version;
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public ProtoVersion build() {
            return new ProtoVersionImpl(this);
        }
    }

}
