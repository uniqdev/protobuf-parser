/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.parser.schema;

import sk.uniq.protobuf.schema.ProtoBasicType;
import sk.uniq.protobuf.schema.ProtoType;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jherkel
 */
public class ProtoTypeTest {

    public ProtoTypeTest() {
    }

    @Test
    public void testGetType() {
        ProtoType type = ProtoType.basicType(ProtoBasicType.STRING);
        assertEquals(type.getBasicType(), ProtoBasicType.STRING);
        assertTrue(type.getMessageType() == null);
        assertTrue(type.getEnumType() == null);
    }

    @Test
    public void testGetMessageType() {
        ProtoType type = ProtoType.messageType("TestType");
        assertEquals(type.getMessageType(), "TestType");
        assertTrue(type.getBasicType() == null);
    }

    @Test
    public void testEnumType() {
        ProtoType type = ProtoType.enumType("TestType");
        assertEquals(type.getEnumType(), "TestType");
        assertTrue(type.getBasicType() == null);
    }

    @Test
    public void testIsBasicType() {
        ProtoType type = ProtoType.basicType(ProtoBasicType.STRING);
        assertTrue(type.getType() == ProtoType.Type.BASIC_TYPE);
        type = ProtoType.messageType("TestType");
        assertTrue(type.getType() != ProtoType.Type.BASIC_TYPE);
    }

    @Test
    public void testBasicType() {
        ProtoType type = ProtoType.basicType(ProtoBasicType.STRING);
        assertEquals(type.getBasicType(), ProtoBasicType.STRING);
        catchException(() -> ProtoType.basicType(null));
        assert caughtException() instanceof IllegalArgumentException;
    }

    @Test
    public void testMessageType() {
        ProtoType type = ProtoType.messageType("TestType");
        assertEquals(type.getMessageType(), "TestType");
        catchException(() -> ProtoType.messageType(null));
        assert caughtException() instanceof IllegalArgumentException;
        catchException(() -> ProtoType.messageType(".A."));
        assert caughtException() instanceof IllegalArgumentException;
    }

    @Test
    public void testIsValidEnumOrMessageType() {
        assertTrue(ProtoType.isValidEnumOrMessageType("A"));
        assertTrue(ProtoType.isValidEnumOrMessageType(".A"));
        assertTrue(ProtoType.isValidEnumOrMessageType(".A.B.C"));
        assertFalse(ProtoType.isValidEnumOrMessageType(""));
        assertFalse(ProtoType.isValidEnumOrMessageType("."));
        assertFalse(ProtoType.isValidEnumOrMessageType("."));
        assertFalse(ProtoType.isValidEnumOrMessageType(".."));
        assertFalse(ProtoType.isValidEnumOrMessageType("A."));
        assertFalse(ProtoType.isValidEnumOrMessageType(".A."));
        assertFalse(ProtoType.isValidEnumOrMessageType(".1"));
        assertFalse(ProtoType.isValidEnumOrMessageType("1"));
    }

}
