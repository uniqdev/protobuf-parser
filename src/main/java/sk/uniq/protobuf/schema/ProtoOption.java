/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

/**
 *
 * @author jherkel
 */
public interface ProtoOption extends ProtoSyntaxElement {

    /**
     *
     */
    public static final String OPTIMIZE_FOR_OPTION = "optimize_for";

    /**
     *
     */
    public static final String JAVA_PACKAGE_OPTION = "java_package";

    /**
     *
     */
    public static final String JAVA_OUTER_CLASSNAME_OPTION = "java_outer_classname";

    /**
     *
     */
    public static final String DEPRECATED_OPTION = "deprecated";

    /**
     *
     */
    public static enum OptimizeForValue {

        /**
         *
         */
        SPEED("SPEED"),
        /**
         *
         */
        CODE_SIZE("CODE_SIZE"),
        /**
         *
         */
        LITE_RUNTIME("LITE_RUNTIME");

        private final String name;

        private OptimizeForValue(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         */
        public String getName() {
            return name;
        }

    }

    public ProtoOptionName getName();

    public ProtoConstant getValue();

}
