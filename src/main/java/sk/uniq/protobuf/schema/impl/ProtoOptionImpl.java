/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import sk.uniq.protobuf.schema.ProtoIdentifier;
import sk.uniq.protobuf.schema.ProtoOption;
import java.util.Collections;
import java.util.Set;
import sk.uniq.protobuf.schema.ProtoBoolean;
import sk.uniq.protobuf.schema.ProtoConstant;
import sk.uniq.protobuf.schema.ProtoOptionName;
import sk.uniq.protobuf.schema.ProtoString;

/**
 *
 * @author jherkel
 */
public final class ProtoOptionImpl extends ProtoSyntaxElementImpl implements ProtoOption {

    private final ProtoOptionName name;
    private final ProtoConstant value;

    private ProtoOptionImpl(Builder builder) {
        super(builder);
        if (builder.name == null) {
            throw new IllegalArgumentException("Invalid name, name = null");
        }
        if (builder.value == null) {
            throw new IllegalArgumentException("Invalid value, value = null");
        }
        this.name = builder.name;
        this.value = builder.value;
    }

    /**
     *
     * @return
     */
    @Override
    public ProtoOptionName getName() {
        return name;
    }

    /**
     *
     * @return
     */
    @Override
    public ProtoConstant getValue() {
        return value;
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.EMPTY_SET;
    }

    @Override
    public ElementType getType() {
        return ElementType.OPTION;
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private ProtoOptionName name;
        private ProtoConstant value;

        private Builder() {
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(ProtoOptionName name) {
            this.name = name;
            return this;
        }

        /**
         *
         * @param value
         * @return
         */
        public Builder value(ProtoConstant value) {
            this.value = value;
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public ProtoOption build() {
            return new ProtoOptionImpl(this);
        }
    }

    ProtoOption optimizeFor(OptimizeForValue value) {
        return builder().name(ProtoOptionNameImpl.fromSimpleName(OPTIMIZE_FOR_OPTION)).value(new ProtoString(value.getName())).build();
    }

    ProtoOption javaPackage(String pkg) {
        return builder().name(ProtoOptionNameImpl.fromSimpleName(JAVA_PACKAGE_OPTION)).value(new ProtoString(pkg)).build();
    }

    ProtoOption javaOuterClassname(String classname) {
        return builder().name(ProtoOptionNameImpl.fromSimpleName(JAVA_OUTER_CLASSNAME_OPTION)).value(new ProtoString(classname)).build();
    }

    ProtoOption deprecated() {
        return builder().name(ProtoOptionNameImpl.fromSimpleName(DEPRECATED_OPTION)).value(new ProtoBoolean(Boolean.TRUE)).build();
    }
}
