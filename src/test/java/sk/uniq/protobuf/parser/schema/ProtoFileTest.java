/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.parser.schema;

import sk.uniq.protobuf.schema.ProtoFile;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import org.junit.Test;
import static org.junit.Assert.*;
import sk.uniq.protobuf.schema.ProtoVersion.Version;
import sk.uniq.protobuf.schema.impl.ProtoFileImpl;

/**
 *
 * @author jherkel
 */
public class ProtoFileTest {

    public ProtoFileTest() {
    }

    @Test
    public void testGetVersion() {
        ProtoFile pf = ProtoFileImpl.builder().filename("test.proto").version(Version.VERSION_3).build();
        assertEquals(Version.VERSION_3, pf.getVersion());

        // test missing version
        catchException(() -> ProtoFileImpl.builder().filename("test.proto").version(null).build());
        assert caughtException() instanceof IllegalArgumentException;

    }

    @Test
    public void testFilename() {
        ProtoFile pf = ProtoFileImpl.builder().filename("test.proto").version(Version.VERSION_3).build();
        assertEquals("test.proto", pf.getFilename());
    }    
    
}
