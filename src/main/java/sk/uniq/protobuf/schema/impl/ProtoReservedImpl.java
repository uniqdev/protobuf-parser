/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import sk.uniq.protobuf.schema.ProtoIdentifier;
import sk.uniq.protobuf.schema.ProtoRange;
import sk.uniq.protobuf.schema.ProtoReserved;

/**
 *
 * @author jherkel
 */
public final class ProtoReservedImpl extends ProtoSyntaxElementImpl implements ProtoReserved {

    private final List<String> names;
    private final List<ProtoRange> ranges;

    private ProtoReservedImpl(Builder builder) {
        super(builder);
        if (builder.names.isEmpty() == true && builder.ranges.isEmpty() == true) {
            throw new IllegalArgumentException("Invalid reserved,  empty tag numbers or field names");
        }
        if (builder.names.isEmpty() == false && builder.ranges.isEmpty() == false) {
            throw new IllegalArgumentException("Invalid reserved, mix field names and tag numbers");
        }
        this.names = Collections.unmodifiableList(builder.names);
        this.ranges = Collections.unmodifiableList(builder.ranges);
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.EMPTY_SET;
    }

    @Override
    public ElementType getType() {
        return ElementType.RESERVED;
    }

    @Override
    public List<String> getNames() {
        return names;
    }

    @Override
    public List<ProtoRange> getRanges() {
        return ranges;
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private final List<String> names = new ArrayList();
        private final List<ProtoRange> ranges = new ArrayList();

        private Builder() {
        }

        /**
         *
         * @return
         */
        @Override
        public ProtoReserved build() {
            return new ProtoReservedImpl(this);
        }

        public Builder addName(String name) {
            if (ProtoIdentifier.isValidIdentifier(name) == false) {
                throw new IllegalStateException("Invalid identifier " + name);
            }
            names.add(name);
            return this;
        }

        public Builder addRange(ProtoRange range) {
            ranges.add(range);
            return this;
        }
    }
}
