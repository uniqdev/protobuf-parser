/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import sk.uniq.protobuf.schema.ProtoEnum;
import sk.uniq.protobuf.schema.ProtoEnumField;
import sk.uniq.protobuf.schema.ProtoIdentifier;

/**
 *
 * @author jherkel
 */
public final class ProtoEnumImpl extends ProtoSyntaxElementImpl implements ProtoEnum {

    private final ProtoIdentifier name;

    private ProtoEnumImpl(Builder builder) {
        super(builder);
        if (builder.name == null) {
            throw new IllegalArgumentException("Invalid name, name = null");
        }
        List<ProtoEnumField> enumFields = getFilteredNestedElements(ElementType.ENUM_FIELD);
        if (enumFields.isEmpty() == false) {
            boolean zeroValue = false;
            for (ProtoEnumField element : enumFields) {
                if (element.getValue() == 0) {
                    zeroValue = true;
                    break;
                }
            }
            if (zeroValue == false) {
                throw new IllegalArgumentException("Enum must contains zero value");
            }
        }
        this.name = builder.name;
    }

    /**
     *
     * @return
     */
    @Override
    public ProtoIdentifier getName() {
        return name;
    }
    
    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.unmodifiableSet(EnumSet.of(ElementType.ENUM_FIELD,ElementType.OPTION));
    }

    @Override
    public ElementType getType() {
        return ElementType.ENUM;
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private ProtoIdentifier name;

        private Builder() {
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(ProtoIdentifier name) {
            this.name = name;
            return this;
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(String name) {
            this.name = ProtoIdentifierImpl.builder().name(name).build();
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public ProtoEnum build() {
            return new ProtoEnumImpl(this);
        }
    }

}
