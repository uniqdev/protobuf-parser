/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

/**
 *
 * @author jherkel
 */
public class ProtoBoolean extends ProtoConstant<Boolean> {

    public ProtoBoolean(boolean value) {
        this(value, Formatter.DEFAULT,null);
    }

    public ProtoBoolean(boolean value, Formatter formatter,String sourceText) {
        super(value, formatter,sourceText);
    }

    @Override
    public Type getType() {
        return Type.BOOLEAN;
    }

    public static ProtoBoolean parse(String str) {
        if ("true".equals(str) == true) {
            return new ProtoBoolean(true,Formatter.DEFAULT,str);
        } else if ("false".equals(str) == true) {
            return new ProtoBoolean(false,Formatter.DEFAULT,str);
        } else {
            throw new IllegalArgumentException("Invalid boolean value " + str);
        }
    }

    public enum Formatter implements ConstantFormatter<Boolean> {
        DEFAULT {
            @Override
            public String toString(Boolean value) {
                return Boolean.toString(value);
            }
        };
    }
}
