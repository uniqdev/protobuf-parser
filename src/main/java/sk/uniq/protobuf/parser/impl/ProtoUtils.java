/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.parser.impl;

import java.util.List;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import sk.uniq.protobuf.grammar.ProtoLexer;
import sk.uniq.protobuf.grammar.ProtoParser.ConstantContext;
import sk.uniq.protobuf.schema.ProtoBoolean;
import sk.uniq.protobuf.schema.ProtoComments;
import sk.uniq.protobuf.schema.ProtoConstant;
import sk.uniq.protobuf.schema.ProtoFloat;
import sk.uniq.protobuf.schema.ProtoInteger;
import sk.uniq.protobuf.schema.ProtoString;

/**
 *
 * @author jherkel
 */
final class ProtoUtils {

    private ProtoUtils() {
    }

    /**
     *
     * @param str
     * @return
     */
    public static String removeQuotes(String str) {
        if (str.length() < 2) {
            throw new IllegalArgumentException("Invalid size str:" + str);
        }
        if ((str.charAt(0) != '\"' && str.charAt(str.length() - 1) != '\"')
                && (str.charAt(0) != '\'' && str.charAt(str.length() - 1) != '\'')) {
            throw new IllegalArgumentException("Invalid quotes str:" + str);
        }
        return str.substring(1, str.length() - 1);
    }

    // Comments which appear to be attached to the previous token are stored
    // in *prev_tailing_comments.  Comments which appear to be attached to the
    // next token are stored in *next_leading_comments.  Comments appearing in
    // between which do not appear to be attached to either will be added to
    // detached_comments.  Any of these parameters can be NULL to simply discard
    // the comments.
    //
    // A series of line comments appearing on consecutive lines, with no other
    // tokens appearing on those lines, will be treated as a single comment.
    //
    // Only the comment content is returned; comment markers (e.g. //) are
    // stripped out.  For block comments, leading whitespace and an asterisk will
    // be stripped from the beginning of each line other than the first.  Newlines
    // are included in the output.
    //
    // Examples:
    //
    //   optional int32 foo = 1;  // Comment attached to foo.
    //   // Comment attached to bar.
    //   optional int32 bar = 2;
    //
    //   optional string baz = 3;
    //   // Comment attached to baz.
    //   // Another line attached to baz.
    //
    //   // Comment attached to qux.
    //   //
    //   // Another line attached to qux.
    //   optional double qux = 4;
    //
    //   // Detached comment.  This is not attached to qux or corge
    //   // because there are blank lines separating it from both.
    public static ProtoComments processComments(BufferedTokenStream tokenStream, ParserRuleContext ctx, boolean addTrailingComment) {
        Token stop = ctx.getStop();
        Token start = ctx.getStart();
        List<Token> tokensBefore = tokenStream.getHiddenTokensToLeft(start.getTokenIndex(), ProtoLexer.HIDDEN);
        if (tokensBefore != null) {
            for (Token token : tokensBefore) {
                if (token.getType() == ProtoLexer.LINE_COMMENT) {
                    System.out.println("Comment");
                }
            }
        }
        if (addTrailingComment) {
            List<Token> tokensAfter = tokenStream.getHiddenTokensToRight(stop.getTokenIndex(), ProtoLexer.HIDDEN);
            if (tokensAfter != null && tokensAfter.size() > 0) {
                for (Token token : tokensAfter) {
                    if (token.getType() == ProtoLexer.LINE_COMMENT) {
                        System.out.println("Comment");
                    } else {
                        if (token.getText().contains("\n")) {
                            break;
                        }
                    }
                }
            }
        }
        return null;
    }

}
