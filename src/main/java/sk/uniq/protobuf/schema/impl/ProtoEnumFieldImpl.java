/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import sk.uniq.protobuf.schema.ProtoIdentifier;
import sk.uniq.protobuf.schema.ProtoOption;
import sk.uniq.protobuf.schema.ProtoEnumField;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import sk.uniq.protobuf.schema.ProtoSchemaException;

/**
 *
 * @author jherkel
 */
public final class ProtoEnumFieldImpl extends ProtoSyntaxElementImpl implements ProtoEnumField {

    private final ProtoIdentifier name;
    private final int value;

    private ProtoEnumFieldImpl(Builder builder) throws ProtoSchemaException {
        super(builder);
        if (builder.name == null) {
            throw new ProtoSchemaException("Invalid name, name = null");
        }
        if (builder.value == null) {
            throw new ProtoSchemaException("Invalid value, value = null");
        }
        this.name = builder.name;
        this.value = builder.value;
    }

    /**
     *
     * @return
     */
    @Override
    public ProtoIdentifier getName() {
        return name;
    }

    /**
     *
     * @return
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.unmodifiableSet(EnumSet.of(ElementType.OPTION));
    }

    @Override
    public ElementType getType() {
        return ElementType.ENUM_FIELD;
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private ProtoIdentifier name;
        private Integer value;

        private Builder() {
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(ProtoIdentifier name) {
            this.name = name;
            return this;
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(String name) {
            this.name = ProtoIdentifierImpl.builder().name(name).build();
            return this;
        }        
        
        /**
         *
         * @param value
         * @return
         */
        public Builder value(Integer value) {
            this.value = value;
            return this;
        }

        /**
         *
         * @param option
         * @return
         */
        public Builder addOption(ProtoOption option) {
            addNestedElement(option);
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public ProtoEnumField build() {
            return new ProtoEnumFieldImpl(this);
        }
    }

}
