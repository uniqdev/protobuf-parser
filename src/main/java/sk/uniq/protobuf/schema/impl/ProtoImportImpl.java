/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import sk.uniq.protobuf.schema.ProtoImport;
import java.util.Collections;
import java.util.Set;
import sk.uniq.protobuf.schema.ProtoImport.Scope;

/**
 *
 * @author jherkel
 */
public class ProtoImportImpl extends ProtoSyntaxElementImpl implements ProtoImport {

    private final Scope scope;
    private final String protoFile;

    private ProtoImportImpl(Builder builder) {
        super(builder);
        if (builder.scope == null) {
            throw new IllegalArgumentException("Unknown import scope, scope == null");
        }
        this.scope = builder.scope;
        this.protoFile = builder.protoFile;
    }

    /**
     *
     * @return
     */
    @Override
    public Scope getScope() {
        return scope;
    }

    /**
     *
     * @return
     */
    @Override
    public String getProtoFile() {
        return protoFile;
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.EMPTY_SET;
    }

    @Override
    public ElementType getType() {
        return ElementType.IMPORT;
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private Scope scope = Scope.PUBLIC;
        private String protoFile;

        private Builder() {
        }

        /**
         *
         * @param scope
         * @return
         */
        public Builder scope(Scope scope) {
            this.scope = scope;
            return this;
        }

        /**
         *
         * @param protoFile
         * @return
         */
        public Builder protoFile(String protoFile) {
            this.protoFile = protoFile;
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public ProtoImport build() {
            return new ProtoImportImpl(this);
        }
    }

}
