/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

/**
 *
 * @author jherkel
 */
public abstract class ProtoConstant<T> {

    public enum Type {
        BOOLEAN,
        INTEGER,
        FLOAT,
        STRING,
        IDENTIFIER
    }

    private final T value;
    private final String sourceText;
    private final ConstantFormatter<T> formatter;

    protected ProtoConstant(T value, ConstantFormatter<T> formatter) {
        this(value, formatter, null);
    }

    protected ProtoConstant(T value, ConstantFormatter<T> formatter, String sourceText) {
        this.value = value;
        this.formatter = formatter;
        this.sourceText = sourceText;
    }

    public abstract Type getType();

    public T getValue() {
        return value;
    }

    public String getFormattedValue() {
        return formatter.toString(value);
    }

    public String getSourceText() {
        return sourceText;
    }

    public ConstantFormatter<T> getFormatter() {
        return formatter;
    }

}
