/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jherkel
 */
public class ProtoStringTest {
    
    public ProtoStringTest() {
    }


    @Test
    public void testParse() {
        ProtoString str = ProtoString.parse("");
        assertEquals(str.getSourceText(),"");
        assertEquals(str.getValue(),"");
        str = ProtoString.parse("test");
        assertEquals(str.getSourceText(),"test");
        assertEquals(str.getValue(),"test");
        str = ProtoString.parse("test\\");
        assertEquals(str.getSourceText(),"test\\");
        assertEquals(str.getValue(),"test\\");
        str = ProtoString.parse("test\\n");
        assertEquals(str.getSourceText(),"test\\n");
        assertEquals(str.getValue(),"test\n");
        str = ProtoString.parse("test\\y");
        assertEquals(str.getSourceText(),"test\\y");
        assertEquals(str.getValue(),"test\\y");
    }
    
}
