/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

import java.math.BigDecimal;

/**
 *
 * @author jherkel
 */
public class ProtoFloat extends ProtoConstant<Double> {

    public ProtoFloat(double value) {
        this(value, Formatter.DEFAULT,null);
    }

    public ProtoFloat(double value, Formatter formatter,String sourceText) {
        super(value, formatter,sourceText);
    }

    @Override
    public Type getType() {
        return Type.FLOAT;
    }

    public static ProtoFloat parse(String text) {
        return parse(text, false);
    }

    public static ProtoFloat parse(String text, boolean minusFlag) {
        if (text.isEmpty()) {
            throw new IllegalArgumentException("Invalid input, empty text");
        }
        if ("inf".equals(text) == true) {
            return new ProtoFloat(minusFlag == true ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY);
        } else if ("nan".equals(text) == true) {
            return new ProtoFloat(Double.NaN);
        } else {
            BigDecimal bd = new BigDecimal(text);
            return new ProtoFloat(minusFlag == true ? -bd.doubleValue() : bd.doubleValue());
        }
    }

    public enum Formatter implements ConstantFormatter<Double> {
        DEFAULT {
            @Override
            public String toString(Double value) {
                return Double.toString(value);
            }
        };
    }
}
