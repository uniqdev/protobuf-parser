/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jherkel
 */
public enum ProtoBasicType {

    /**
     *
     */
    DOUBLE("double") {
        @Override
        public Object defaultValue() {
            return (double) 0.0;
        }

    },

    /**
     *
     */
    FLOAT("float") {
        @Override
        public Object defaultValue() {
            return (float) 0.0f;
        }

    },

    /**
     *
     */
    INT32("int32") {
        @Override
        public Object defaultValue() {
            return (int) 0;
        }
    },

    /**
     *
     */
    INT64("int64") {
        @Override
        public Object defaultValue() {
            return (long) 0;
        }
    },

    /**
     *
     */
    UINT32("uint32") {
        @Override
        public Object defaultValue() {
            return (int) 0;
        }
    },

    /**
     *
     */
    UINT64("uint64") {
        @Override
        public Object defaultValue() {
            return (long) 0;
        }
    },

    /**
     *
     */
    SINT32("sint32") {
        @Override
        public Object defaultValue() {
            return (int) 0;
        }
    },

    /**
     *
     */
    SINT64("sint64") {
        @Override
        public Object defaultValue() {
            return (long) 0;
        }
    },

    /**
     *
     */
    FIXED32("fixed32") {
        @Override
        public Object defaultValue() {
            return (int) 0;
        }
    },

    /**
     *
     */
    FIXED64("fixed64") {
        @Override
        public Object defaultValue() {
            return (long) 0;
        }
    },

    /**
     *
     */
    SFIXED32("sfixed32") {
        @Override
        public Object defaultValue() {
            return (int) 0;
        }
    },

    /**
     *
     */
    SFIXED64("sfixed64") {
        @Override
        public Object defaultValue() {
            return (long) 0;
        }
    },

    /**
     *
     */
    BOOL("bool") {
        @Override
        public Object defaultValue() {
            return Boolean.FALSE;
        }
    },

    /**
     *
     */
    STRING("string") {
        @Override
        public Object defaultValue() {
            return "";
        }
    },

    /**
     *
     */
    BYTES("bytes") {
        @Override
        public Object defaultValue() {
            return EMPTY_BYTES;
        }
    };

    private static final byte[] EMPTY_BYTES = new byte[0];
    private static final Map<String,ProtoBasicType> VALUES;

    /**
     * Holds the defined one byte identifier for the type.
     */
    private final String identifier;

    /**
     * Creates an instance of type from its defined one byte identifier.
     *
     * @param identifier The one byte identifier for the type.
     */
    private ProtoBasicType(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Extracts the byte identifier for the type.
     *
     * @return The byte identifier for the type.
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Converts an instance of the type to an equivalent Java native
     * representation.
     *
     *
     * @return An equivalent Java native representation.
     */
    public abstract Object defaultValue();

    /**
     *
     * @param type
     * @return
     */
    public static boolean isAllowedInMapKey(ProtoBasicType type) {
        switch (type) {
            case INT32:
            case INT64:
            case UINT32:
            case UINT64:
            case SINT32:
            case SINT64:
            case FIXED32:
            case FIXED64:
            case SFIXED32:
            case SFIXED64:
            case BOOL:
            case STRING:
                return true;
            default:
                return false;
        }
    }
    
    static {
        ProtoBasicType[] tmpValues = values();
        Map<String,ProtoBasicType> mapTmp = new HashMap<>();
        for (ProtoBasicType basicType : tmpValues) {
            mapTmp.put(basicType.getIdentifier(), basicType);
        }
        VALUES = Collections.unmodifiableMap(mapTmp);
    }

    public static ProtoBasicType fromString(String text) {
        return VALUES.get(text);
    }

}
