/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.parser;

import static org.junit.Assert.assertFalse;
import org.junit.Test;
import sk.uniq.protobuf.parser.impl.ProtoParserImpl;

/**
 *
 * @author jherkel
 */
public class ProtoGeneratorTest extends BaseProtoTest {

    public ProtoGeneratorTest() {
    }

    @Test
    public void testGenerate() {
        TestErrorListener errorListener = new TestErrorListener();
        ProtoParser instance = new ProtoParserImpl("test_basic.proto", ProtoParserOptions.DEBUG);
        instance.addListener(errorListener);
        ProtoParserResult result = instance.parse(testBasicProto);
        assertFalse(errorListener.hasError());
        String generatorResult = ProtoGenerator.generate(result.getProtoFile(), ProtoGenerator.PRETTY_PRINT);
        ProtoParser instance2 = new ProtoParserImpl("", ProtoParserOptions.DEBUG);
        errorListener.clear();
        instance2.addListener(errorListener);
        ProtoParserResult result2 = instance2.parse(generatorResult);
        assertFalse(errorListener.hasError());
    }

}
