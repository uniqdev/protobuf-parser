/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.parser;

import java.io.File;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Test;
import sk.uniq.protobuf.parser.impl.ProtoParserImpl;

/**
 *
 * @author jherkel
 */
public class ProtoParserTest extends BaseProtoTest{

    public ProtoParserTest() {
    }

    @Test
    public void testParseBasic() throws IOException {

        ProtoParser instance = new ProtoParserImpl(null, ProtoParserOptions.DEBUG);
        TestErrorListener errorListener = new TestErrorListener();
        instance.addListener(errorListener);
        ProtoParserResult result = instance.parse(testBasicProto);
        assertFalse(errorListener.hasError());
    }

    @Test
    public void testParseFromFile() throws IOException {
        ProtoParser instance = new ProtoParserImpl("test_basic.proto", ProtoParserOptions.DEBUG);
        TestErrorListener errorListener = new TestErrorListener();
        instance.addListener(errorListener);
        ProtoParserResult result = instance.parse(new File(ProtoParserTest.class.getResource("test_basic.proto").getFile()));
        assertFalse(errorListener.hasError());
    }

}
