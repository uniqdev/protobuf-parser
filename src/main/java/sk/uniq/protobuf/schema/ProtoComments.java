/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author jherkel
 */
public final class ProtoComments {

    /**
     *
     */
    public static final ProtoComments EMPTY = builder().build();

    private final List<ProtoComment> preceding;
    private final List<ProtoComment> orphans;
    private final List<ProtoComment> following;

    private ProtoComments(Builder builder) {
        preceding = builder.preceding == null ? Collections.EMPTY_LIST : Collections.unmodifiableList(builder.preceding);
        orphans = builder.orphans == null ? Collections.EMPTY_LIST : Collections.unmodifiableList(builder.orphans);
        following = builder.following == null ? Collections.EMPTY_LIST : Collections.unmodifiableList(builder.following);
    }

    /**
     *
     * @return
     */
    public List<ProtoComment> getPreceding() {
        return preceding;
    }

    /**
     *
     * @return
     */
    public List<ProtoComment> getOrphans() {
        return orphans;
    }

    /**
     *
     * @return
     */
    public List<ProtoComment> getFollowing() {
        return following;
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     *
     */
    public static final class Builder {

        private List<ProtoComment> preceding;
        private List<ProtoComment> orphans;
        private List<ProtoComment> following;

        private Builder() {
        }

        /**
         *
         * @param comment
         * @return
         */
        public Builder addPreceding(ProtoComment comment) {
            if (preceding == null) {
                preceding = new LinkedList<>();
            }
            preceding.add(comment);
            return this;
        }

        /**
         *
         * @param comment
         * @return
         */
        public Builder addOrphan(ProtoComment comment) {
            if (orphans == null) {
                orphans = new LinkedList<>();
            }
            orphans.add(comment);
            return this;
        }

        /**
         *
         * @param comment
         * @return
         */
        public Builder addFollowing(ProtoComment comment) {
            if (following == null) {
                following = new LinkedList<>();
            }
            following.add(comment);
            return this;
        }

        /**
         *
         * @return
         */
        public ProtoComments build() {
            return new ProtoComments(this);
        }
    }

}
