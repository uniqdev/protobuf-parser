/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import sk.uniq.protobuf.schema.ProtoMessage;
import sk.uniq.protobuf.schema.ProtoIdentifier;
import sk.uniq.protobuf.schema.ProtoMap;
import sk.uniq.protobuf.schema.ProtoMessageField;
import sk.uniq.protobuf.schema.ProtoOneOf;
import sk.uniq.protobuf.schema.ProtoOneOfField;
import sk.uniq.protobuf.schema.ProtoSchemaException;
import sk.uniq.protobuf.schema.ProtoSyntaxElement;

/**
 *
 * @author jherkel
 */
public final class ProtoMessageImpl extends ProtoSyntaxElementImpl implements ProtoMessage {

    private final ProtoIdentifier name;

    private ProtoMessageImpl(Builder builder) {
        super(builder);
        if (builder.name == null) {
            throw new IllegalArgumentException("Invalid name, name = null");
        }
        List<ProtoSyntaxElement> tmp = getFilteredNestedElements(EnumSet.of(ElementType.MESSAGE_FIELD, ElementType.MAP,ElementType.ONEOF));
        if (tmp.isEmpty() == false) {
            Set<Integer> fieldNumbers = new HashSet<>(tmp.size());
            for (ProtoSyntaxElement element : tmp) {
                switch (element.getType()) {
                    case MESSAGE_FIELD:
                        if (fieldNumbers.add(((ProtoMessageField) element).getFieldNumber()) == false) {
                            throw new ProtoSchemaException("Duplicate field number:" + ((ProtoMessageField) element).getFieldNumber(), element);
                        }
                        break;
                    case MAP:
                        if (fieldNumbers.add(((ProtoMap) element).getFieldNumber()) == false) {
                            throw new ProtoSchemaException("Duplicate field number:" + ((ProtoMap) element).getFieldNumber(), element);
                        }
                        break;
                    case ONEOF:
                        for (ProtoOneOfField oneOfField : ((ProtoOneOf) element).<ProtoOneOfField>getFilteredNestedElements(ElementType.ONEOF_FIELD)) {
                            if (fieldNumbers.add(oneOfField.getFieldNumber()) == false) {
                                throw new ProtoSchemaException("Duplicate field number:" + oneOfField.getFieldNumber(), oneOfField);
                            }
                            break;
                        }
                }
            }
        }
        this.name = builder.name;
    }

    /**
     *
     * @return
     */
    @Override
    public ProtoIdentifier getName() {
        return name;
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.unmodifiableSet(EnumSet.of(ElementType.ENUM, ElementType.MESSAGE, ElementType.MAP, 
                ElementType.MESSAGE_FIELD, ElementType.OPTION,ElementType.ONEOF, ElementType.RESERVED));
    }

    @Override
    public ElementType getType() {
        return ElementType.MESSAGE;
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private ProtoIdentifier name;

        private Builder() {
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(ProtoIdentifier name) {
            this.name = name;
            return this;
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(String name) {
            this.name = ProtoIdentifierImpl.builder().name(name).build();
            return this;
        }

        /**
         *
         * @return 
         * @throws ProtoSchemaException
         */
        @Override
        public ProtoMessage build() {
            return new ProtoMessageImpl(this);
        }

    }
}
