/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

import sk.uniq.protobuf.schema.impl.ProtoIdentifierImpl;

/**
 *
 * @author jherkel
 */
public class ProtoConstantIdentifier extends ProtoConstant<ProtoIdentifier> {

    public ProtoConstantIdentifier(ProtoIdentifier value, Formatter formatter, String sourceText) {
        super(value, formatter, sourceText);
    }

    @Override
    public ProtoConstant.Type getType() {
        return ProtoConstant.Type.IDENTIFIER;
    }

    public static ProtoConstantIdentifier parse(String str) {
        return new ProtoConstantIdentifier(ProtoIdentifierImpl.builder().name(str).position(ProtoPosition.EMPTY_POSITION).build(), Formatter.DEFAULT, str);
    }

    public enum Formatter implements ConstantFormatter<ProtoIdentifier> {
        DEFAULT {
            @Override
            public String toString(ProtoIdentifier value) {
                return value.getName();
            }
        };
    }
}
