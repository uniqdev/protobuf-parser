/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import sk.uniq.protobuf.schema.ProtoMap;
import sk.uniq.protobuf.schema.ProtoFieldNumber;
import sk.uniq.protobuf.schema.ProtoType;
import java.util.Collections;
import java.util.Set;
import sk.uniq.protobuf.schema.ProtoBasicType;
import sk.uniq.protobuf.schema.ProtoIdentifier;

/**
 *
 * @author jherkel
 */
public final class ProtoMapImpl extends ProtoSyntaxElementImpl implements ProtoMap {

    private final ProtoIdentifier name;
    private final ProtoType keyType;
    private final ProtoType valueType;
    private final int fieldNumber;

    private ProtoMapImpl(Builder builder) {
        super(builder);
        if (builder.name == null) {
            throw new IllegalArgumentException("Invalid name, name = null");
        }
        if (builder.keyType == null) {
            throw new IllegalArgumentException("Invalid keyType, keyType = null");
        }
        if (builder.keyType.getType() != ProtoType.Type.BASIC_TYPE || ProtoBasicType.isAllowedInMapKey(builder.keyType.getBasicType()) == false) {
            throw new IllegalArgumentException("Invalid keyType, not allowed type keyType:" + builder.keyType.toString());
        }
        if (builder.valueType == null) {
            throw new IllegalArgumentException("Invalid name, valueType = null");
        }
        if (ProtoFieldNumber.isValid(builder.fieldNumber) == false) {
            throw new IllegalArgumentException("Invalid field number, fieldNumber:" + builder.fieldNumber);
        }

        this.name = builder.name;
        this.keyType = builder.keyType;
        this.valueType = builder.valueType;
        this.fieldNumber = builder.fieldNumber;
    }

    /**
     *
     * @return
     */
    @Override
    public ProtoType getKeyType() {
        return keyType;
    }

    /**
     *
     * @return
     */
    @Override
    public ProtoType getValueType() {
        return valueType;
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.EMPTY_SET;
    }

    @Override
    public ElementType getType() {
        return ElementType.MAP;
    }

    @Override
    public int getFieldNumber() {
        return fieldNumber;
    }

    @Override
    public ProtoIdentifier getName() {
        return name;
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private ProtoIdentifier name;
        private ProtoType keyType;
        private ProtoType valueType;
        private int fieldNumber = ProtoFieldNumber.UNINTIALIZED;

        private Builder() {
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(ProtoIdentifier name) {
            this.name = name;
            return this;
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(String name) {
            this.name = ProtoIdentifierImpl.builder().name(name).build();
            return this;
        }

        /**
         *
         * @param keyType
         * @return
         */
        public Builder keyType(ProtoType keyType) {
            this.keyType = keyType;
            return this;
        }

        /**
         *
         * @param valueType
         * @return
         */
        public Builder valueType(ProtoType valueType) {
            this.valueType = valueType;
            return this;
        }

        /**
         *
         * @param fieldNumber
         * @return
         */
        public Builder fieldNumber(int fieldNumber) {
            this.fieldNumber = fieldNumber;
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public ProtoMap build() {
            return new ProtoMapImpl(this);
        }
    }

}
