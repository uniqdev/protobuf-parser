/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

/**
 *
 * @author jherkel
 */
public interface ProtoIdentifier extends ProtoSyntaxElement {

    public String getName();
    
    public String[] getIdentifiers();
    
    public boolean isFullIdentifier();

    /**
     *
     * @param identifier identifier to be tested
     * @return true if identifier is valid
     */
    public static boolean isValidIdentifier(String identifier) {
        if (identifier.isEmpty() == true) {
            return false;
        }
        if (Character.isLetter(identifier.charAt(0)) == false) {
            return false;
        }
        for (int k = 1; k < identifier.length(); k++) {
            if (Character.isLetter(identifier.charAt(k)) == false && Character.isDigit(identifier.charAt(k)) == false && identifier.charAt(k) != '_') {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param identifier identifier to be tested
     * @return true if identifier is valid
     */
    public static boolean isValidFullIdentifier(String identifier) {
        if (identifier.isEmpty() == true) {
            return false;
        }
        String[] elements = identifier.split(".");
        for (String element : elements) {
            if (isValidIdentifier(element) == false) {
                return false;
            }
        }
        return true;
    }
}
