/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

import static sk.uniq.protobuf.schema.ProtoSchemaVisitor.visitInternal;

/**
 *
 * @author jherkel
 */
public class ProtoBaseSchemaVisitor<T> implements ProtoSchemaVisitor<T> {

    public void process(ProtoSyntaxElement startElement, T context) throws Exception {
        visitInternal(this, startElement, context);
    }

    protected void visitChildren(ProtoSyntaxElement element, T context) throws Exception {
        for (ProtoSyntaxElement pse : element.getNestedElements()) {
            visitInternal(this, pse, context);
        }
    }

    @Override
    public void visit(ProtoFile file, T context) throws Exception {
        visitChildren(file, context);
    }

    @Override
    public void visit(ProtoVersion version, T context) throws Exception {
    }

    @Override
    public void visit(ProtoPackage packagePB, T context) throws Exception {
    }

    @Override
    public void visit(ProtoEnum enumPB, T context) throws Exception {
        visitChildren(enumPB, context);
    }

    @Override
    public void visit(ProtoEnumField enumField, T context) throws Exception {
    }

    @Override
    public void visit(ProtoIdentifier identifier, T context) throws Exception {
    }

    @Override
    public void visit(ProtoImport importPB, T context) throws Exception {
        visitChildren(importPB, context);
    }

    @Override
    public void visit(ProtoMap map, T context) throws Exception {
        visitChildren(map, context);
    }

    @Override
    public void visit(ProtoMessage message, T context) throws Exception {
        visitChildren(message, context);
    }

    @Override
    public void visit(ProtoMessageField messageField, T context) throws Exception {
        visitChildren(messageField, context);
    }

    @Override
    public void visit(ProtoOption option, T context) throws Exception {
    }

    @Override
    public void visit(ProtoReserved reserved, T context) throws Exception {
        visitChildren(reserved, context);
    }

    @Override
    public void visit(ProtoRpc rpc, T context) throws Exception {
        visitChildren(rpc, context);
    }

    @Override
    public void visit(ProtoService service, T context) throws Exception {
        visitChildren(service, context);
    }

    @Override
    public void visit(ProtoOneOf oneof, T context) throws Exception {
        visitChildren(oneof, context);
    }

    @Override
    public void visit(ProtoOneOfField oneOfField, T context) throws Exception {
        visitChildren(oneOfField, context);
    }

}
