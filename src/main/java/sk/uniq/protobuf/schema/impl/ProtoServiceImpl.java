/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import sk.uniq.protobuf.schema.ProtoIdentifier;
import sk.uniq.protobuf.schema.ProtoService;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

/**
 *
 * @author jherkel
 */
public final class ProtoServiceImpl extends ProtoSyntaxElementImpl implements ProtoService {

    private final ProtoIdentifier name;

    private ProtoServiceImpl(Builder builder) {
        super(builder);
        if (builder.name == null) {
            throw new IllegalArgumentException("Invalid name, name = null");
        }
        this.name = builder.name;
    }

    /**
     *
     * @return name of service
     */
    @Override
    public ProtoIdentifier getName() {
        return name;
    }

    /**
     *
     * @return new ProtoService Builder
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.unmodifiableSet(EnumSet.of(ElementType.RPC,ElementType.OPTION));
    }

    @Override
    public ElementType getType() {
        return ElementType.SERVICE;
    }

    /**
     *
     */
    public final static class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private ProtoIdentifier name;

        private Builder() {
        }

        /**
         *
         * @param name of service
         * @return ProtoService Builder
         */
        public Builder name(ProtoIdentifier name) {
            this.name = name;
            return this;
        }

        /**
         *
         * @param name name of service
         * @return ProtoService Builder
         */
        public Builder name(String name) {
            this.name = ProtoIdentifierImpl.builder().name(name).build();
            return this;
        }

        /**
         *
         * @return new ProtoService
         */
        @Override
        public ProtoService build() {
            return new ProtoServiceImpl(this);
        }
    }

}
