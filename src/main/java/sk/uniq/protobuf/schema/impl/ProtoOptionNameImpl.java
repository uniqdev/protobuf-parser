/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import sk.uniq.protobuf.schema.ProtoIdentifier;
import sk.uniq.protobuf.schema.ProtoOptionName;

/**
 *
 * @author jherkel
 */
public class ProtoOptionNameImpl extends ProtoSyntaxElementImpl implements ProtoOptionName {

    private final List<ProtoIdentifier> identifiers;
    private final boolean enclosed;

    private ProtoOptionNameImpl(Builder builder) {
        super(builder);
        this.identifiers = Collections.unmodifiableList(builder.identifiers);
        this.enclosed = builder.enclosed;
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.EMPTY_SET;
    }

    @Override
    public ElementType getType() {
        return ElementType.OPTION_NAME;
    }

    @Override
    public List<ProtoIdentifier> getIdentifiers() {
        return identifiers;
    }

    @Override
    public String getName() {
        StringBuilder sb = new StringBuilder();
        if (identifiers.get(0).isFullIdentifier() == false) {
            sb.append("(").append(identifiers.get(0).getName()).append(")");
        } else {
            sb.append(identifiers.get(0).getName());
        }
        for (int k = 1; k < identifiers.size(); k++) {
            sb.append(".").append(identifiers.get(k).getName());
        }
        return sb.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean isEnclosed() {
        return enclosed;
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private final List<ProtoIdentifier> identifiers = new ArrayList<>(1);
        private boolean enclosed = false;

        private Builder() {
        }
        
        
        /**
         *
         * @param enclosed
         * @return
         */
        public Builder enclosed(boolean enclosed) {
            this.enclosed = enclosed;
            return this;
        }
        
        /**
         *
         * @param identifier
         * @return
         */
        public Builder addIdentifier(ProtoIdentifier identifier) {
            this.identifiers.add(identifier);
            return this;
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder addIdentifier(String name) {
            this.identifiers.add(ProtoIdentifierImpl.builder().name(name).build());
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public ProtoOptionName build() {
            return new ProtoOptionNameImpl(this);
        }
    }
    
    public static ProtoOptionName fromSimpleName(String name) {
        return builder().addIdentifier(name).build();
    }

}
