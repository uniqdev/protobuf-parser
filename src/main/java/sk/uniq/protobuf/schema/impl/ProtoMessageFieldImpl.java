/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import sk.uniq.protobuf.schema.ProtoIdentifier;
import sk.uniq.protobuf.schema.ProtoMessageField;
import sk.uniq.protobuf.schema.ProtoFieldNumber;
import sk.uniq.protobuf.schema.ProtoType;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

/**
 *
 * @author jherkel
 */
public class ProtoMessageFieldImpl extends ProtoSyntaxElementImpl implements ProtoMessageField {

    private final ProtoIdentifier name;
    private final int fieldNumber;
    private final ProtoType fieldType;
    private final boolean repeated;

    public ProtoMessageFieldImpl(Builder builder) {
        super(builder);
        if (builder.name == null) {
            throw new IllegalArgumentException("Invalid name, name = null");
        }
        if (builder.fieldType == null) {
            throw new IllegalArgumentException("Invalid field type, fieldType = null");
        }
        if (ProtoFieldNumber.isValid(builder.fieldNumber) == false) {
            throw new IllegalArgumentException("Invalid field number, fieldNumber:" + builder.fieldNumber);
        }

        this.name = builder.name;
        this.fieldNumber = builder.fieldNumber;
        this.fieldType = builder.fieldType;
        this.repeated = builder.repeated;
    }

    public boolean isRepeated() {
        return repeated;
    }

    /**
     *
     * @return
     */
    @Override
    public ProtoType getFieldType() {
        return fieldType;
    }

    /**
     *
     * @return
     */
    @Override
    public int getFieldNumber() {
        return fieldNumber;
    }

    /**
     *
     * @return
     */
    @Override
    public ProtoIdentifier getName() {
        return name;
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.unmodifiableSet(EnumSet.of(ElementType.OPTION));
    }

    @Override
    public ElementType getType() {
        return ElementType.MESSAGE_FIELD;
    }
    
    /**
     *
     * @return
     */
    public static ProtoMessageFieldImpl.Builder builder() {
        return new Builder();
    }

    /**
     *
     */
    public static class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private ProtoIdentifier name;
        private ProtoType fieldType;
        private int fieldNumber = ProtoFieldNumber.UNINTIALIZED;
        private boolean repeated = false;

        /**
         *
         * @param name
         * @return
         */
        public Builder name(ProtoIdentifier name) {
            this.name = name;
            return this;
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(String name) {
            this.name = ProtoIdentifierImpl.builder().name(name).build();
            return this;
        }

        /**
         *
         * @param repeated
         * @return
         */
        public Builder repeated(boolean repeated) {
            this.repeated = repeated;
            return this;
        }

        /**
         *
         * @param fieldNumber
         * @return
         */
        public Builder fieldNumber(int fieldNumber) {
            this.fieldNumber = fieldNumber;
            return this;
        }

        /**
         *
         * @param fieldType
         * @return
         */
        public Builder fieldType(ProtoType fieldType) {
            this.fieldType = fieldType;
            return this;
        }

        @Override
        public ProtoMessageField build() {
            return new ProtoMessageFieldImpl(this);
        }

    }

}
