/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema.impl;

import sk.uniq.protobuf.schema.ProtoIdentifier;
import sk.uniq.protobuf.schema.ProtoType;
import sk.uniq.protobuf.schema.ProtoRpc;
import java.util.Collections;
import java.util.Set;

/**
 *
 * @author jherkel
 */
public final class ProtoRpcImpl extends ProtoSyntaxElementImpl implements ProtoRpc {

    private final ProtoIdentifier name;
    private final ProtoType messageType;
    private final ProtoType returnType;
    private final boolean messageTypeStream;
    private final boolean returnTypeStream;

    private ProtoRpcImpl(Builder builder) {
        super(builder);
        if (builder.name == null) {
            throw new IllegalArgumentException("Invalid name, name = null");
        }
        if (builder.messageType == null) {
            throw new IllegalArgumentException("Invalid message type, messageType = null");
        }
        if (builder.returnType == null) {
            throw new IllegalArgumentException("Invalid message type, messageType = null");
        }
        if (builder.messageType.getType() != ProtoType.Type.MESSAGE_TYPE) {
            throw new IllegalArgumentException("Invalid message type, messageType must be a message type");
        }
        if (builder.returnType.getType() != ProtoType.Type.MESSAGE_TYPE) {
            throw new IllegalArgumentException("Invalid return type, returnType must be a message type");
        }
        this.name = builder.name;
        this.messageType = builder.messageType;
        this.returnType = builder.returnType;
        this.messageTypeStream = builder.messageTypeStream;
        this.returnTypeStream = builder.returnTypeStream;
    }

    /**
     *
     * @return
     */
    @Override
    public ProtoIdentifier getName() {
        return name;
    }

    @Override
    public ProtoType getMessageType() {
        return messageType;
    }

    @Override
    public ProtoType getReturnType() {
        return returnType;
    }
    
    @Override
    public boolean isMessageTypeStream() {
        return messageTypeStream;
    }

    @Override
    public boolean isReturnTypeStream() {
        return returnTypeStream;
    }

    /**
     *
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Set<ElementType> getAllowedElements() {
        return Collections.EMPTY_SET;
    }

    @Override
    public ElementType getType() {
        return ElementType.RPC;
    }

    /**
     *
     */
    public static final class Builder extends ProtoSyntaxElementImpl.Builder<Builder> {

        private ProtoIdentifier name;
        private ProtoType messageType;
        private ProtoType returnType;
        private boolean messageTypeStream;
        private boolean returnTypeStream;

        private Builder() {
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(ProtoIdentifier name) {
            this.name = name;
            return this;
        }

        /**
         *
         * @param name
         * @return
         */
        public Builder name(String name) {
            this.name = ProtoIdentifierImpl.builder().name(name).build();
            return this;
        }

        /**
         *
         * @param messageType
         * @return
         */
        public Builder messageType(ProtoType messageType) {
            this.messageType = messageType;
            return this;
        }

        /**
         *
         * @param returnType
         * @return
         */
        public Builder returnType(ProtoType returnType) {
            this.returnType = returnType;
            return this;
        }

        /**
         *
         * @param messageTypeStream
         * @return
         */
        public Builder messageTypeStream(boolean messageTypeStream) {
            this.messageTypeStream = messageTypeStream;
            return this;
        }

        /**
         *
         * @param returnTypeStream
         * @return
         */
        public Builder returnTypeStream(boolean returnTypeStream) {
            this.returnTypeStream = returnTypeStream;
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public ProtoRpc build() {
            return new ProtoRpcImpl(this);
        }
    }
}
