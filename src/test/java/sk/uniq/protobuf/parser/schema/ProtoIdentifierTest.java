/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.parser.schema;

import sk.uniq.protobuf.schema.ProtoIdentifier;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import org.junit.Test;
import static org.junit.Assert.*;
import static sk.uniq.protobuf.schema.ProtoIdentifier.isValidIdentifier;
import static sk.uniq.protobuf.schema.impl.ProtoIdentifierImpl.builder;

/**
 *
 * @author jherkel
 */
public class ProtoIdentifierTest {

    public ProtoIdentifierTest() {
    }

    @Test
    public void testConstructor() {
        ProtoIdentifier ident1 = builder().name("test23").build();
        assertEquals("test23", ident1.getName());
        // test invalid name
        catchException(() -> builder().name("2aaa").build());
        assert caughtException() instanceof IllegalArgumentException;
    }

    @Test
    public void testGetName() {
        ProtoIdentifier ident1 = builder().name("test23").build();
        assertEquals("test23", ident1.getName());
    }

    @Test
    public void testIsValidIdentifier() {
        // test valid identifiers
        assertTrue(isValidIdentifier("test23"));
        assertTrue(isValidIdentifier("T"));
        assertTrue(isValidIdentifier("T_"));
        assertTrue(isValidIdentifier("T_12"));
        assertTrue(isValidIdentifier("T__"));
        // test invalid identifiers
        assertFalse(isValidIdentifier(""));
        assertFalse(isValidIdentifier("1"));
        assertFalse(isValidIdentifier("12"));
        assertFalse(isValidIdentifier("#"));
    }

}
