/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

/**
 *
 * @author jherkel
 */
public final class ProtoString extends ProtoConstant<String> {

    public ProtoString(String value) {
        this(value, Formatter.DEFAULT, null);
    }

    public ProtoString(String value, ConstantFormatter formatter, String sourceText) {
        super(value, formatter, sourceText);
    }

    @Override
    public Type getType() {
        return Type.STRING;
    }

    public static ProtoString parse(String str) {
        int sl = str.length();
        StringBuilder out = new StringBuilder(sl);
        for (int i = 0; i < sl; i++) {
            char ch = str.charAt(i);
            if (ch == '\\' && i + 1 < sl) {
                ch = str.charAt(++i);
                switch (ch) {
                    case 'a':
                        out.append(0x7);
                        break;
                    case 'b':
                        out.append('\b');
                        break;
                    case 'f':
                        out.append('\f');
                        break;
                    case 'n':
                        out.append('\n');
                        break;
                    case 'r':
                        out.append('\r');
                        break;
                    case 't':
                        out.append('\t');
                        break;
                    case 'v':
                        out.append(0xb);
                        break;
                    case '\'':
                        out.append('\'');
                        break;
                    case '\"':
                        out.append('\"');
                        break;
                    case 'x':
                    case 'X':
                        if (i < sl + 2) {
                            int hex1 = Character.digit(str.charAt(i + 1), 16);
                            int hex2 = Character.digit(str.charAt(i + 2), 16);
                            if (hex1 != -1 && hex2 != -1) {
                                out.append((char) hex1 * 16 + hex2);
                            }
                        }
                        break;
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                        if (i < sl + 3) {
                            int octal1 = Character.digit(str.charAt(i + 1), 8);
                            int octal2 = Character.digit(str.charAt(i + 2), 8);
                            int octal3 = Character.digit(str.charAt(i + 3), 8);
                            if (octal1 != -1 && octal1 != -1 && octal3 != -1) {
                                out.append((char) octal1 * 64 + octal2 * 8 + octal3);
                            }
                        }
                        break;
                    default:
                        out.append('\\').append(ch);
                        break;
                }
            } else {
                out.append(ch);
            }
        }
        String tmpStr = out.toString();
        return new ProtoString(tmpStr, Formatter.DEFAULT, str);
    }

    public enum Formatter implements ConstantFormatter<String> {
        DEFAULT {
            @Override
            public String toString(String value) {
                return value;
            }
        }

    }

}
