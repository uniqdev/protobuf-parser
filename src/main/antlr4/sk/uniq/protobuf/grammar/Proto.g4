// Compatibility with Protocol Buffer defines
// https://developers.google.com/protocol-buffers/docs/reference/proto3-spec

grammar Proto;

proto : syntax ( importPB | packagePB | option | topLevelDef | emptyStatement )* EOF;

topLevelDef : message | enumPB | service;

service : SERVICE_LITERAL serviceName BLOCK_OPEN  (option | rpc | emptyStatement)* BLOCK_CLOSE;

rpc : RPC_LITERAL rpcName PAREN_OPEN (argStream=STREAM_LITERAL)? argType=messageType PAREN_CLOSE RETURNS_LITERAL PAREN_OPEN (retStream=STREAM_LITERAL)? retType=messageType PAREN_CLOSE (( BLOCK_OPEN (option | emptyStatement)* BLOCK_CLOSE ) | ITEM_TERMINATOR );

message : MESSAGE_LITERAL messageName messageBody;
messageBody : BLOCK_OPEN ( field | enumPB | message | option | oneof | mapField | reserved | emptyStatement )* BLOCK_CLOSE;

enumPB : ENUM_LITERAL enumName enumBody;
enumBody : BLOCK_OPEN ( option | enumField | emptyStatement )* BLOCK_CLOSE;
enumField : name=IDENTIFIER ASSIGN value=INTEGER_LITERAL ( BRACKET_OPEN enumValueOption ( COMMA  enumValueOption )* BRACKET_CLOSE )? ITEM_TERMINATOR ;
enumValueOption : optionName ASSIGN constant;


reserved : RESERVED_LITERAL ( ranges | fieldNames ) ITEM_TERMINATOR;
fieldNames : reservedFieldName ( COMMA reservedFieldName)*;
reservedFieldName : STRING_LITERAL;

ranges : range (COMMA range )*;
range : INTEGER_LITERAL ( EXTENSIONS_TO_LITERAL ( INTEGER_LITERAL | EXTENSIONS_MAX_LITERAL ) )?;

mapField : MAP_LITERAL MAP_OPEN mapKeyType COMMA type MAP_CLOSE mapName ASSIGN fieldNumber ( BRACKET_OPEN fieldOptions BRACKET_CLOSE )? ITEM_TERMINATOR ;

oneof : ONEOF_LITERAL oneofName BLOCK_OPEN ( oneofField | emptyStatement )* BLOCK_CLOSE;
oneofField : type fieldName ASSIGN fieldNumber ( BRACKET_OPEN fieldOptions BRACKET_CLOSE )? ITEM_TERMINATOR ;

field : REPEATED_LITERAL? type fieldName ASSIGN fieldNumber ( BRACKET_OPEN  fieldOptions BRACKET_CLOSE )? ITEM_TERMINATOR ;
fieldOptions : fieldOption ( COMMA  fieldOption )*;
fieldOption : optionName ASSIGN constant;

type : protoType | messageOrEnumType;
fieldNumber : INTEGER_LITERAL;

option : OPTION_LITERAL optionName ASSIGN constant ITEM_TERMINATOR ;
optionName : ( IDENTIFIER | PAREN_OPEN fullIdentifier PAREN_CLOSE ) ( DOT IDENTIFIER )*;

packagePB : PACKAGE_LITERAL fullIdentifier ITEM_TERMINATOR;

importPB : IMPORT_LITERAL ( WEAK_LITERAL | PUBLIC_LITERAL )? protoFile=STRING_LITERAL ITEM_TERMINATOR ; 

syntax : SYNTAX_LITERAL ASSIGN version=STRING_LITERAL ITEM_TERMINATOR ;

constant : fullIdentifier | ( ( PLUS | MINUS )? INTEGER_LITERAL ) | ( ( MINUS | PLUS )? FLOAT_LITERAL ) | (STRING_LITERAL | BOOLEAN_LITERAL);

mapKeyType : (INT32_TYPE_LITERAL
  |  INT64_TYPE_LITERAL
  |  UINT32_TYPE_LITERAL
  |  UINT64_TYPE_LITERAL
  |  SINT32_TYPE_LITERAL
  |  SINT64_TYPE_LITERAL 
  |  FIXED32_TYPE_LITERAL
  |  FIXED64_TYPE_LITERAL
  |  SFIXED32_TYPE_LITERAL
  |  SFIXED64_TYPE_LITERAL
  |  BOOLEAN_TYPE_LITERAL
  |  STRING_TYPE_LITERAL)
  ;

protoType : (DOUBLE_TYPE_LITERAL
  |  FLOAT_TYPE_LITERAL
  |  INT32_TYPE_LITERAL
  |  INT64_TYPE_LITERAL
  |  UINT32_TYPE_LITERAL
  |  UINT64_TYPE_LITERAL
  |  SINT32_TYPE_LITERAL
  |  SINT64_TYPE_LITERAL 
  |  FIXED32_TYPE_LITERAL
  |  FIXED64_TYPE_LITERAL
  |  SFIXED32_TYPE_LITERAL
  |  SFIXED64_TYPE_LITERAL
  |  BOOLEAN_TYPE_LITERAL
  |  STRING_TYPE_LITERAL
  |  BYTES_TYPE_LITERAL)
  ;

fullIdentifier : IDENTIFIER (DOT IDENTIFIER)*;
messageName : IDENTIFIER;
enumName : IDENTIFIER;
fieldName : IDENTIFIER;
oneofName : IDENTIFIER;
mapName : IDENTIFIER;
serviceName : IDENTIFIER;
rpcName : IDENTIFIER;
messageOrEnumType : (DOT)? (IDENTIFIER DOT)* IDENTIFIER;
messageType : (DOT)? (IDENTIFIER DOT)* messageName;
enumType : (DOT)? (IDENTIFIER DOT)* enumName;

emptyStatement : ITEM_TERMINATOR;

// LEXER

SYNTAX_LITERAL: 'syntax';
PACKAGE_LITERAL : 'package' ;
IMPORT_LITERAL : 'import' ;
OPTION_LITERAL : 'option' ;

ENUM_LITERAL : 'enum' ;
MESSAGE_LITERAL : 'message' ;
ONEOF_LITERAL : 'oneof';
MAP_LITERAL : 'map' ;
EXTENSIONS_TO_LITERAL : 'to' ;
EXTENSIONS_MAX_LITERAL : 'max' ;
REPEATED_LITERAL : 'repeated' ;
RESERVED_LITERAL : 'reserved' ;

SERVICE_LITERAL : 'service' ;
RETURNS_LITERAL : 'returns' ;
RPC_LITERAL : 'rpc' ;
STREAM_LITERAL : 'stream';
PUBLIC_LITERAL : 'public';
WEAK_LITERAL : 'weak';

BLOCK_OPEN : '{' ;
BLOCK_CLOSE : '}' ;
PAREN_OPEN : '(' ;
PAREN_CLOSE : ')' ;
BRACKET_OPEN : '[' ;
BRACKET_CLOSE : ']' ;
MAP_OPEN : '<' ;
MAP_CLOSE : '>' ;
ASSIGN : '=' ;
COLON : ':' ;
COMMA : ',' ;
DOT : '.' ;
PLUS : '+' ;
MINUS : '-' ;
QUOTE : '"' ;
ITEM_TERMINATOR : ';' ;

DOUBLE_TYPE_LITERAL : 'double' ;
FLOAT_TYPE_LITERAL : 'float' ;
INT32_TYPE_LITERAL : 'int32' ;
INT64_TYPE_LITERAL : 'int64' ;
UINT32_TYPE_LITERAL : 'uint32' ;
UINT64_TYPE_LITERAL : 'uint64' ;
SINT32_TYPE_LITERAL : 'sint32' ;
SINT64_TYPE_LITERAL : 'sint64' ;
FIXED32_TYPE_LITERAL : 'fixed32' ;
FIXED64_TYPE_LITERAL : 'fixed64' ;
SFIXED32_TYPE_LITERAL : 'sfixed32' ;
SFIXED64_TYPE_LITERAL : 'sfixed64' ;
BOOLEAN_TYPE_LITERAL : 'bool' ;
STRING_TYPE_LITERAL : 'string' ;
BYTES_TYPE_LITERAL : 'bytes' ;

STRING_LITERAL : ('\'' CHAR_VALUE* '\'') | ('\"' CHAR_VALUE* '\"');
fragment CHAR_VALUE : HEX_ESCAPE | OCTAL_ESCAPE | CHAR_ESCAPE | ~('\\'|'\"'|'\n'|'\r');
fragment HEX_ESCAPE : '\\' HEX_PREFIX HEX_DIGIT HEX_DIGIT;
fragment OCTAL_ESCAPE : '\\' OCTAL_DIGIT OCTAL_DIGIT OCTAL_DIGIT;
fragment CHAR_ESCAPE : '\\' ( 'a' | 'b' | 'f' | 'n' | 'r' | 't' | 'v' | '\\' | '\'' | '\"' );

BOOLEAN_LITERAL : 'true' | 'false';

FLOAT_LITERAL : (DECIMALS DOT DECIMALS? EXPONENT? | DECIMALS EXPONENT | DOT DECIMALS EXPONENT?) | 'inf' | 'nan';
fragment DECIMALS : DECIMAL_DIGIT (DECIMAL_DIGIT)*;
fragment EXPONENT : ('e' | 'E') (PLUS | MINUS)? DECIMAL_DIGIT+ ;

INTEGER_LITERAL :  HEX_LITERAL |  OCTAL_LITERAL |  DECIMAL_LITERAL; 

fragment DECIMAL_LITERAL : '1'..'9' (DECIMAL_DIGIT)*;
fragment OCTAL_LITERAL  : '0' (OCTAL_DIGIT)*;
fragment HEX_LITERAL : '0' HEX_PREFIX HEX_DIGIT (HEX_DIGIT)*;

fragment HEX_PREFIX : 'x' | 'X';

IDENTIFIER : LETTER (LETTER | DECIMAL_DIGIT | '_')* ;

fragment LETTER : ('a'..'z' | 'A'..'Z' );
fragment DECIMAL_DIGIT : '0'..'9';
fragment OCTAL_DIGIT : '0'..'7';
fragment HEX_DIGIT : ('0'..'9' | 'A'..'F' | 'a'..'f');

LINE_COMMENT
    : '//' ~[\r\n]* '\r'? '\n' -> channel(HIDDEN)
    ;
    
WHITESPACE 
    : (' ' | '\t' | '\u000C')+ -> channel(HIDDEN)
    ;

NEWLINE
    : '\r'? '\n' -> channel(HIDDEN)
    ;

ERR_CHAR
  : . -> channel(HIDDEN)
  ;