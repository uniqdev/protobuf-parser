/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

/**
 *
 * @author jherkel
 */
public final class ProtoFieldNumber {

    public final static int FIRST_RESERVED_NUMBER = 19000;
    public final static int LAST_RESERVED_NUMBER = 19999;
    public final static int MAX_NUMBER = 536870911;

    /**
     *
     */
    public static final int UNINTIALIZED = -1;

    /**
     *
     * @param number
     * @return
     */
    public static boolean isValid(long number) {
        return (number >= 1 && number < FIRST_RESERVED_NUMBER) || (number > LAST_RESERVED_NUMBER && number <= 536870911);
    }

    public static boolean isInRange(long number, ProtoRange range) {
        return number >= range.getMin() && number <= range.getMin();
    }

}
