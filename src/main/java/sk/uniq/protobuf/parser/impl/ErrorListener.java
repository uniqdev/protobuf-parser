/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.parser.impl;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import sk.uniq.protobuf.parser.ProtoParserError;
import sk.uniq.protobuf.schema.ProtoPosition;
import sk.uniq.protobuf.parser.ProtoErrorListener;

/**
 *
 * @author jherkel
 */
/**
 *
 * @author jherkel
 */
class ErrorListener implements org.antlr.v4.runtime.ANTLRErrorListener {

    private final List<ProtoErrorListener> errorListeners;
    private final List<ProtoParserError> errors = new ArrayList<>();

    public ErrorListener(List<ProtoErrorListener> errorListeners) {
        this.errorListeners = new ArrayList<>(errorListeners);
    }

    public List<ProtoParserError> getErrors() {
        return errors;
    }

    public boolean hasError() {
        return !errors.isEmpty();
    }

    public void addError(ProtoPosition position,String message) {
        ProtoParserError error = new ProtoParserError(position, message);
        errorListeners.forEach((errorListener) -> errorListener.protobufError(error));
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        int startPos = 0,endPos = 0,columnEnd = charPositionInLine;
        if (offendingSymbol != null) {
            Token t = (Token)offendingSymbol;
            startPos = t.getStartIndex();
            endPos = t.getStopIndex();
            columnEnd = charPositionInLine + t.getText().length();
        } 
        ProtoPosition position = new ProtoPosition(startPos, endPos, line, charPositionInLine,columnEnd);
        ProtoParserError error = new ProtoParserError(position, msg);
        errorListeners.forEach((errorListener) -> errorListener.protobufError(error));
    }

    @Override
    public void reportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, boolean exact, BitSet ambigAlts, ATNConfigSet configs) {
    }

    @Override
    public void reportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, ATNConfigSet configs) {
    }

    @Override
    public void reportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, ATNConfigSet configs) {
    }
    
}
