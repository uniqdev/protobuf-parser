/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.parser.impl;

import sk.uniq.protobuf.parser.ProtoGenerator;
import sk.uniq.protobuf.schema.ProtoBaseSchemaVisitor;
import sk.uniq.protobuf.schema.ProtoConstant;
import sk.uniq.protobuf.schema.ProtoEnum;
import sk.uniq.protobuf.schema.ProtoEnumField;
import sk.uniq.protobuf.schema.ProtoFile;
import sk.uniq.protobuf.schema.ProtoImport;
import sk.uniq.protobuf.schema.ProtoOption;
import sk.uniq.protobuf.schema.ProtoSchemaVisitor;
import sk.uniq.protobuf.schema.ProtoSyntaxElement;
import sk.uniq.protobuf.schema.ProtoVersion;

/**
 *
 * @author jherkel
 */
public class DefaultProtoGenerator extends ProtoBaseSchemaVisitor<ProtoSyntaxElement> {

    private static final String TERMINATOR = ";";
    private final ProtoGenerator.Options options;
    private final StringBuilder output = new StringBuilder();

    public DefaultProtoGenerator(ProtoGenerator.Options options) {
        this.options = options;
    }

    @Override
    public void visit(ProtoFile file, ProtoSyntaxElement context) throws Exception {
        if (options.getHeader() != null && options.getHeader().isEmpty() == false) {
            output.append(options.getHeader());
            newline();
        }
        super.visit(file, file);
    }

    @Override
    public void visit(ProtoVersion version, ProtoSyntaxElement context) throws Exception {
        output.append("syntax=\"")
                .append(version.getVersion().getName())
                .append("\";");
        newline();
    }

    @Override
    public void visit(ProtoEnum enumPB, ProtoSyntaxElement context) throws Exception {
        output.append("enum ")
                .append(enumPB.getName().getName())
                .append("{");
        super.visit(enumPB, enumPB);
        output.append("}");
        newline();
    }

    @Override
    public void visit(ProtoEnumField enumField, ProtoSyntaxElement context) throws Exception {
        output.append(enumField.getName().getName())
                .append("=")
                .append(enumField.getValue());
        if (enumField.getFilteredNestedElements(ProtoSyntaxElement.ElementType.OPTION).isEmpty() == false) {
            output.append(" [");
            boolean start = true;
            for (ProtoOption option : enumField.<ProtoOption>getFilteredNestedElements(ProtoSyntaxElement.ElementType.OPTION)) {
                if (start == false) {
                    output.append(",");
                }
                ProtoSchemaVisitor.visitInternal(this, option, option);
                start = false;
            }
            output.append("]");
        }
        output.append(TERMINATOR);
        newline();
    }

    @Override
    public void visit(ProtoImport importPB, ProtoSyntaxElement context) throws Exception {
        output.append("import ")
                .append(importPB.getScope().getName())
                .append(" ")
                .append(importPB.getProtoFile())
                .append(TERMINATOR);
        newline();
    }

    @Override
    public void visit(ProtoOption option, ProtoSyntaxElement context) throws Exception {
        switch (context.getType()) {
            case ENUM_FIELD:
            case ONEOF_FIELD:
            case MESSAGE_FIELD:
                break;
            default:
                output.append("option ");
                break;
        }
        if (option.getName().isEnclosed() == true) {
            output.append("(").append(option.getName().getName()).append(")");
        } else {
            output.append(option.getName().getName());

        }
        output.append("=");
        if (option.getValue().getType() == ProtoConstant.Type.STRING) {
            output.append("\"")
                    .append(option.getValue().getSourceText())
                    .append("\"");

        } else {
            output.append(option.getValue().getSourceText());
        }
        switch (context.getType()) {
            case ENUM_FIELD:
            case ONEOF_FIELD:
            case MESSAGE_FIELD:
                break;
            default:
                output.append(TERMINATOR);
                newline();
                break;
        }
    }

    public String getResult() {
        return output.toString();
    }

    private void newline() {
        if (options.isPrettyPrint() == true) {
            output.append(options.getLineSeparator());
        }
    }

}
