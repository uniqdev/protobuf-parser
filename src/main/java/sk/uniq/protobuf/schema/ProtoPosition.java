/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

/**
 *
 * @author jherkel
 */
public final class ProtoPosition {

    /**
     *
     */
    public static final ProtoPosition EMPTY_POSITION = new ProtoPosition();
    
    private final int startPos;
    private final int endPos;
    private final int line;
    private final int columnStart;
    private final int columnEnd;

    private ProtoPosition() {
        startPos = endPos = line = columnStart = columnEnd = 0;
    }
    
    /**
     *
     * @param startPos
     * @param endPos
     * @param line
     * @param columnStart
     * @param columnEnd
     */
    public ProtoPosition(int startPos, int endPos, int line, int columnStart, int columnEnd) {
        if (endPos < startPos) {
            throw new IllegalArgumentException("endPos < startPos startPos:" + startPos + " endPos:" + endPos);
        }
        this.startPos = startPos;
        this.endPos = endPos;
        this.line = line;
        this.columnStart = columnStart;
        this.columnEnd = columnEnd;
    }

    /**
     *
     * @return
     */
    public int getStartPos() {
        return startPos;
    }

    /**
     *
     * @return
     */
    public int getEndPos() {
        return endPos;
    }

    /**
     *
     * @return
     */
    public int getColumnStart() {
        return columnStart;
    }

    /**
     *
     * @return
     */
    public int getColumnEnd() {
        return columnEnd;
    }

    /**
     *
     * @return
     */
    public int getLine() {
        return line;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "ProtoPosition{" + "startPos:" + startPos + ", endPos:" + endPos + ", line:" + line + ", columnStart:" + columnStart + ", columnEnd:" + columnEnd + '}';
    }

    
}
