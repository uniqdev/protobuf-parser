/* 
 * Copyright 2016 Jakub Herkel.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sk.uniq.protobuf.schema;

/**
 *
 * @author jherkel
 */
public final class ProtoType {

    public enum Type {
        BASIC_TYPE,
        MESSAGE_TYPE,
        ENUM_TYPE,
        MESSAGE_OR_ENUM_TYPE
    }

    private final ProtoBasicType basicType;
    private final String enumOrMessageType;
    private final Type type;

    private ProtoType(ProtoBasicType basicType) {
        if (basicType == null) {
            throw new IllegalArgumentException("Invalid basic type, no type defined");
        }
        this.basicType = basicType;
        this.enumOrMessageType = null;
        type = Type.BASIC_TYPE;
    }

    private ProtoType(String enumOrMessageType, Type type) {
        if (enumOrMessageType == null) {
            throw new IllegalArgumentException("Invalid type, no type defined");
        }
        if (isValidEnumOrMessageType(enumOrMessageType) == false) {
            throw new IllegalArgumentException("Invalid enum or message identifier");
        }
        this.basicType = null;
        this.enumOrMessageType = enumOrMessageType;
        this.type = type;
    }

    /**
     *
     * @return basic type or null if type isn't a basic type
     */
    public ProtoBasicType getBasicType() {
        return basicType;
    }

    /**
     *
     * @return message type or null if type is a basic type or enum type
     */
    public String getMessageType() {
        return enumOrMessageType != null && (type == Type.MESSAGE_TYPE || type == Type.MESSAGE_OR_ENUM_TYPE) ? enumOrMessageType : null;
    }

    /**
     *
     * @return enum type or null if type is a basic type or message type
     */
    public String getEnumType() {
        return enumOrMessageType != null && (type == Type.ENUM_TYPE || type == Type.MESSAGE_OR_ENUM_TYPE) ? enumOrMessageType : null;
    }

    /**
     *
     * @return true if type is a message type
     */
    public Type getType() {
        return type;
    }
  
    /**
     *
     * @param type
     * @return
     */
    public static ProtoType basicType(ProtoBasicType type) {
        return new ProtoType(type);
    }

    /**
     *
     * @param enumType
     * @return
     */
    public static ProtoType enumType(String enumType) {
        return new ProtoType(enumType, Type.ENUM_TYPE);
    }

    /**
     *
     * @param messageType
     * @return
     */
    public static ProtoType messageType(String messageType) {
        return new ProtoType(messageType, Type.MESSAGE_TYPE);
    }

    /**
     *
     * @param messageOrEnumType
     * @return
     */
    public static ProtoType messageOrEnumType(String messageOrEnumType) {
        return new ProtoType(messageOrEnumType, Type.MESSAGE_OR_ENUM_TYPE);
    }

    /**
     * Check if type has a correct form (DOT)? (IDENTIFIER DOT)* IDENTIFIER;
     *
     * @param type type to be tested
     * @return true if type is valid
     */
    public static boolean isValidEnumOrMessageType(String type) {
        if (type.isEmpty() == true) {
            return false;
        }
        int startIdent = 0;
        int lastDot = -1;
        for (int k = 0; k < type.length(); k++) {
            if (type.charAt(k) == '.') {
                // ignore dot at position 0
                if (k > 1) {
                    if (lastDot >= 0 && lastDot + 1 == k) {
                        return false;
                    }
                    if (ProtoIdentifier.isValidIdentifier(type.substring(startIdent, k)) == false) {
                        return false;
                    }
                }
                startIdent = k + 1;
                lastDot = k;
            }
        }
        if (lastDot == type.length() - 1) {
            return false;
        }
        if (ProtoIdentifier.isValidIdentifier(type.substring(startIdent, type.length())) == false) {
            return false;
        }
        return true;
    }

}
